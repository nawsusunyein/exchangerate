//
//  ExchangeRateListViewController.swift
//  CBM Exchange
//
//  Created by techfun on 2018/01/23.
//  Copyright © 2018 techfun. All rights reserved.
//

import UIKit
import AlamofireObjectMapper
import Alamofire
import SDWebImage
import os.log

class ExchangeRateListViewController: UIViewController , UITableViewDataSource, UITableViewDelegate{
    
    @IBOutlet weak var menuBtn: UIBarButtonItem!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var exchangeView: UIView!
    
    // View variables for show and hide.
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var loadingStatus: UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var retryBtn: UIButton!
    
    var exchangeRateList = ExchangeRateList()
    
    var initialLanguage : Language?
    var chosenLanguage = Language()
    var chooseLanguage = ""
    let inputview = UIView(frame : CGRect(x : 0 , y : 50 , width : 240 , height : 100))
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // For Menu.
        if self.revealViewController() != nil
        {
            menuBtn.target = self.revealViewController()
            menuBtn.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        dateView.cardView()
        exchangeView.cardView()
        
        // To know whether language is stored.
        initialLanguage = LanguageHelper.loadLanguage(isRequire: false)
        if(initialLanguage == nil)
        {
            loadingView.isHidden = true
            contentView.isHidden = true
            chooseAlert()
        }
        else
        {
            // API call for exchange list.
            getExchangeRateList()
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    // For Table View To Show Exchange Rate.
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return exchangeRateList!.rates.keys.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellidentifier = "exchangeRateListCellid"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellidentifier, for:indexPath ) as? ExchangeRateListTableViewCell else
        {
            fatalError("The cell is not instance of ExchangeRateListTableViewCell")
        }
        
        let key = Array(exchangeRateList!.rates.keys)[indexPath.row]
        cell.currencyTypeLabel.text = key
        cell.exchangeRateLabel.text = exchangeRateList!.rates[key]
        cell.currencyImage.sd_setImage(with: URL(string:UsedURL.URL.domainUrl + UsedURL.Endpoints.imageLoadedUrl + key + UsedURL.Endpoints.imageExtension),placeholderImage: UIImage(named: "denyPhoto.png"))
        return cell
    }
    
    // To show alert for choosing language.
    func chooseAlert(){
        //let titleEnglish = "titleEN".localized()
        //let titleMyanmar = "titleMM".localized()
        let titleEnglish = "English"
        let titleMyanmar = "မြန်မာ"
        let alert = UIAlertController(title : "chooseLanguage".localized() ,message : "\n\n\n\n\n\n" , preferredStyle:.alert)
        let firstRadioButton = createRadioButton(frame: CGRect(x: 100, y: 0, width: alert.view.frame.width/2, height: 50), title: titleEnglish, color: UIColor.black);
        let secondRadioButton = createRadioButton(frame: CGRect(x:100, y: 50, width: alert.view.frame.width/2, height: 50), title: titleMyanmar, color: UIColor.black);
        firstRadioButton.otherButtons = [secondRadioButton]
        alert.view.addSubview(inputview)
        alert.addAction(UIAlertAction(title : "OK",style: .default , handler : {
            (_) in
            
            if(secondRadioButton.isSelected)
            {
                self.chooseLanguage = LanguageType.MM_Lang.rawValue
            }
            else
            {
                self.chooseLanguage = LanguageType.EN_Lang.rawValue
            }
            self.chosenLanguage = Language(language : self.chooseLanguage)
            LanguageHelper.saveLanguage(lang : self.chosenLanguage)
            self.getExchangeRateList()
            let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
            
            //To indicate home view when user have chosen language
            rootviewcontroller.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "rootnav")
        }))
        self.present(alert,animated: true, completion: nil)
    }
    
    // To create radio button.
    private func createRadioButton(frame : CGRect, title : String, color : UIColor) -> DLRadioButton {
        let radioButton = DLRadioButton(frame: frame);
        radioButton.titleLabel!.font = UIFont.systemFont(ofSize: 14);
        radioButton.setTitle(title, for: []);
        radioButton.setTitleColor(color, for: []);
        radioButton.iconColor = color;
        radioButton.indicatorColor = color;
        radioButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left;
        inputview.addSubview(radioButton);
        return radioButton;
    }
    
    //MARK: DatePicker from TextField to Choose Date
    @IBAction func dateTextFieldChange(_ sender: AnyObject)
    {
        let alert = UIAlertController(title : "chooseDate".localized() , message : "\n\n\n\n\n\n\n\n\n\n",preferredStyle:.alert)
        let inputView = UIView(frame : CGRect(x : 0 , y : 0 , width : alert.view.frame.width , height : 240))
        let datePickerView = UIDatePicker(frame : CGRect(x : 0 , y:0 , width : 250 , height : 240))
        datePickerView.maximumDate = Date()
        datePickerView.datePickerMode = UIDatePickerMode.date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let date = dateFormatter.date(from:dateTextField.text!)
        datePickerView.date = date!
        alert.addAction(UIAlertAction(title : "Cancel" , style : .cancel , handler : nil))
        alert.addAction(UIAlertAction(title : "OK" , style : .default , handler : {
            (_) in
            self.getExchangeRateList()
        }))
        datePickerView.addTarget(self, action: #selector(ExchangeRateListViewController.dateFieldChanged(sender:)), for: UIControlEvents.valueChanged)
        inputView.addSubview(datePickerView)
        alert.view.addSubview(inputView)
        self.present(alert,animated:true,completion:nil)
    }
    
    //MARK: To put user chosen date in textfield
    func dateFieldChanged(sender : UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateTextField.text = dateFormatter.string(from: sender.date)
    }
    
    //MARK: Call API to show Exchange List
    func getExchangeRateList()
    {
        let loading = "loading"
        let updating = "updatedData"
        let nodata = "noDataAvailable"
        let somethingWrong = "somethingWrong"
        let networkError = "networkError"
        var URL = ""
        if(self.dateTextField.text?.isEmpty)!
        {
            URL = UsedURL.URL.domainUrl + UsedURL.Endpoints.apiLatestUrl
            loadingView.isHidden = false
            loadingStatus.isHidden = false
            loadingIndicator.isHidden = false
            retryBtn.isHidden = true
            contentView.isHidden = true
            
        }
        else
        {
            URL = UsedURL.URL.domainUrl + UsedURL.Endpoints.apiHistoryUrl + dateTextField.text!
            ProgressHUD.show(loading.localized() , interaction : false )
        }
        Alamofire.request(URL, method :.get).responseObject{(response:DataResponse<ExchangeRateList>) in
            guard response.result.error == nil else
            {
                print(response.result.error ?? "Something Went Wrong")
                if(self.dateTextField.text?.isEmpty)!
                {
                    self.loadingStatus.text = somethingWrong.localized()
                    self.retryBtn.isHidden = true
                }
                else
                {
                    ProgressHUD.showError(somethingWrong.localized())
                }
                return
            }
            
            guard let value = response.result.value else
            {
                print("Network Error")
                if(self.dateTextField.text?.isEmpty)!
                {
                    self.loadingStatus.text = networkError.localized()
                    self.retryBtn.isHidden = true
                }else
                {
                    ProgressHUD.showError(networkError.localized())
                }
                return
            }
            
            if value.info == "Central Bank of Myanmar"
            {
                self.exchangeRateList = value
                self.tableView.reloadData()
                if(self.dateTextField.text?.isEmpty)!
                {
                    self.loadingStatus.isHidden = false
                    self.loadingIndicator.isHidden = false
                    self.loadingView.isHidden = false
                    let timeval = self.exchangeRateList?.timestamp
                    let timestamp = Int(timeval!)
                    let date = Date(timeIntervalSinceNow: TimeInterval(timestamp!)/100000)
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy"
                    self.dateTextField.text = dateFormatter.string(from: date)
                    self.contentView.isHidden = false
                }
                else
                {
                    if(self.exchangeRateList?.rates.isEmpty)!
                    {
                        ProgressHUD.showSuccess(nodata.localized())
                    }
                    else
                    {
                        ProgressHUD.showSuccess(updating.localized())
                    }
                }
                
            }
            else{}
        }
    }
    
    
    //MARK: To go to exchange rate detail view
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        switch (segue.identifier ?? "") {
        case "exchangeRateDetailShowID" :
            guard let destinationController = segue.destination as? ExchangeRateDetailViewController else
            {
                fatalError("Unexpected destination view controller \(segue.destination)")
            }
            guard let selectedCell = sender as? ExchangeRateListTableViewCell else
            {
                fatalError("Unexpected sender \(segue.source)")
            }
            destinationController.selectedExchange = [(selectedCell.currencyTypeLabel.text!) : (selectedCell.exchangeRateLabel.text!)]
        default :
            fatalError("Unexpected segue identifier \(String(describing: segue.identifier))")
        }
    }
}
