//
//  ExchangeRateList.swift
//  CBM Exchange
//
//  Created by techfun on 2018/01/23.
//  Copyright © 2018 techfun. All rights reserved.
//

import Foundation
import ObjectMapper
class ExchangeRateList : Mappable {
    
    var info : String = ""
    var description : String?
    var rates = [String : String]()
    var timestamp : String = ""
    
    required init?(map : Map)
    {
        
    }
    required init?()
    {
        
    }
    
    func mapping (map : Map)
    {
        info <- map["info"]
        description <- map["description"]
        timestamp <- map["timestamp"]
        rates <- map["rates"]
    }
}
