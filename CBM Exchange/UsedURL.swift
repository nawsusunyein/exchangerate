//
//  UsedURL.swift
//  CBM Exchange
//
//  Created by techfun on 2018/01/26.
//  Copyright © 2018 techfun. All rights reserved.
//

import Foundation

class UsedURL{
    struct URL
    {
        static let domainUrl = "https://forex.cbm.gov.mm"
    }
    
    struct Endpoints
    {
        
        static let apiLatestUrl = "/api/latest"
        static let apiHistoryUrl = "/api/history/"
        static let apiCurrencyUrl = "/api/currencies"
        static let imageLoadedUrl = "/template/img/flag/"
        static let imageExtension = "_24.png"
        
    }
    
    
}


enum LanguageType : String {
    
    case EN_Lang = "en-US"
    case MM_Lang = "my-MM"
}
