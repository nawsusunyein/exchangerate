//
//  StringExtension.swift
//  CBM Exchange
//
//  Created by techfun on 2018/01/25.
//  Copyright © 2018 techfun. All rights reserved.
//

import Foundation

extension String{
    
    func localized() -> String{
        
        // Read from store en_US, mm_MM
        var lang : String?
        let chooseLang = LanguageHelper.loadLanguage(isRequire: true)
        lang = chooseLang?.language
        let path = Bundle.main.path(forResource: lang , ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
        
    }
}
