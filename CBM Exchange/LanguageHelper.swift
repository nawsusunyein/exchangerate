//
//  LanguageHelper.swift
//  CBM Exchange
//
//  Created by techfun on 2018/01/24.
//  Copyright © 2018 techfun. All rights reserved.
//

import Foundation
import os.log

class LanguageHelper
{
    static var language : Language?
    
    let sampleView = UIViewController()
    static func loadLanguage(isRequire : Bool) -> Language?
    {
        
        if language != nil
        {
            return language
        }
        
        language = NSKeyedUnarchiver.unarchiveObject(withFile: Language.ArchiveURL.path) as? Language
        
        if (language == nil && isRequire)
        {
            return Language(language: "en-US")
        }
        return language
    }
    
    static func saveLanguage(lang : Language){
        
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(lang, toFile: Language.ArchiveURL.path)
        if isSuccessfulSave
        {
            os_log("Language set successfully saved.", log: OSLog.default, type: .debug)
            LanguageHelper.language = lang
            SettingViewController.setAppleLAnguageTo(lang: (lang.language)! )
            
        }
        else
        {
            os_log("Failed to save language set...", log: OSLog.default, type: .error)
        }
    }
}
