//
//  ExchangeRateDetailViewController.swift
//  CBM Exchange
//
//  Created by techfun on 2018/01/23.
//  Copyright © 2018 techfun. All rights reserved.
//

import UIKit
import AlamofireObjectMapper
import Alamofire
class ExchangeRateDetailViewController: UIViewController {
    
    
    @IBOutlet weak var currencyCardView: UIView!
    
    // Variable to receive currency type and exchange rate from selected cell.
    var selectedExchange : [String : String]?
    @IBOutlet weak var currencyTypeLbl: UILabel!
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var valueLbl: UILabel!
    @IBOutlet weak var exchangeLbl: UILabel!
    
    // Array to check value is 100/=K or 1/=K .
    let valueRate = ["JPY","KHR","IDR","KRW","LAK","VND"]
    
    // Dictionary declaration to receive list of currency types and country.
    var currencyList : [String : String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currencyCardView.cardView()
        getCurrencyandCountry()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    // To go back to exchange rate list .
    
    @IBAction func goback(_ sender: AnyObject)
    {
        let isPresentingInAddNoteMode = presentingViewController is UINavigationController
        if isPresentingInAddNoteMode
        {
            dismiss(animated: true, completion: nil)
        }
        else if let presentingnoteinedit = navigationController
        {
            presentingnoteinedit.popViewController(animated: true)
        }
        else
        {
            fatalError("NoteviewController is not in navigatioin controller")
        }
    }
    
    // MARK: Call API to get country and currencies
    func getCurrencyandCountry(){
        Alamofire.request(UsedURL.URL.domainUrl + UsedURL.Endpoints.apiCurrencyUrl , method : .get).responseObject{(response : DataResponse<CurrencyList>) in
            guard response.result.error == nil else
            {
                print(response.result.error ?? "Error")
                return
            }
            guard let value = response.result.value else
            {
                print("Network Error")
                return
            }
            if value.info == "Central Bank of Myanmar"
            {
                self.currencyList = value.currencies
                for (key,value) in self.selectedExchange!
                {
                    self.currencyTypeLbl.text = key
                    self.exchangeLbl.text = value
                    
                    if self.valueRate.contains(key)
                    {
                        self.valueLbl.text = "100/=K"
                    }
                    else
                    {
                        self.valueLbl.text = "1/=K"
                    }
                }
                
                for (key,value) in self.currencyList!
                {
                    if key == self.currencyTypeLbl.text
                    {
                        self.countryLbl.text = value
                        self.navigationItem.title = value
                    }
                    
                }
            }
        }
    }
    
}
