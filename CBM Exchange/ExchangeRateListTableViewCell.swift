//
//  ExchangeRateListTableViewCell.swift
//  CBM Exchange
//
//  Created by techfun on 2018/01/23.
//  Copyright © 2018 techfun. All rights reserved.
//

import UIKit

class ExchangeRateListTableViewCell: UITableViewCell {
    
    // Label for currency type and exchange rate
    @IBOutlet weak var currencyTypeLabel: UILabel!
    @IBOutlet weak var exchangeRateLabel: UILabel!
    @IBOutlet weak var currencyImage: UIImageView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
}
