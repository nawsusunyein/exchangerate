//
//  Card.swift
//  CBM Exchange
//
//  Created by techfun on 2018/01/23.
//  Copyright © 2018 techfun. All rights reserved.
//

import Foundation


extension UIView {
    
    // For card view
    func cardView()
    {
        // corner radius
        self.layer.cornerRadius = CGFloat(2.0)
        
        // shadow
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = 1.0
        self.layer.masksToBounds = false
    }
}
