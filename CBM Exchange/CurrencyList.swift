//
//  CurrencyList.swift
//  CBM Exchange
//
//  Created by techfun on 2018/01/24.
//  Copyright © 2018 techfun. All rights reserved.
//

import Foundation
import ObjectMapper

class CurrencyList : Mappable{
    
    var info : String = ""
    var description : String?
    var currencies = [String : String]()
    
    required init?(map: Map)
    {
        
    }
    required init?()
    {
        
    }
    
    func mapping(map: Map)
    {
        info <- map["info"]
        description <- map["description"]
        currencies <- map["currencies"]
    }
}
