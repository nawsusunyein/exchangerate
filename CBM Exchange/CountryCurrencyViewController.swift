//
//  CountryCurrencyViewController.swift
//  CBM Exchange
//
//  Created by techfun on 2018/01/23.
//  Copyright © 2018 techfun. All rights reserved.
//

import UIKit
import AlamofireObjectMapper
import Alamofire
import SDWebImage

class CountryCurrencyViewController: UIViewController , UITableViewDataSource , UITableViewDelegate{
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var menuBtn: UIBarButtonItem!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loadingStatus: UILabel!
    @IBOutlet weak var retryBtn: UIButton!
    
    // To store  currency list and country list from api.
    var currencyList = CurrencyList()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // For menu appearance.
        if self.revealViewController() != nil {
            menuBtn.target = self.revealViewController()
            menuBtn.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        
        getCurrencyList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    // For Table View to show list of currency types and countries.
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return currencyList!.currencies.keys.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellidentifier = "currencyReuseIdentifier"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellidentifier, for: indexPath) as? CurrencyListTableViewCell else
        {
            fatalError("The cell is no instance of CurrencyListTableViewCell")
        }
        
        let key = Array(currencyList!.currencies.keys)[indexPath.row]
        cell.currencyTypeLbl.text = key
        cell.countryLbl.text = currencyList!.currencies[key]
        cell.currencyImage.sd_setImage(with: URL(string: UsedURL.URL.domainUrl + UsedURL.Endpoints.imageLoadedUrl + key + UsedURL.Endpoints.imageExtension),placeholderImage: UIImage(named: "denyPhoto.png"))
        return cell
    }
    
    
    //MARK: Call API to get currency and country list.
    func getCurrencyList()
    {
        self.loadingView.isHidden = false
        self.loadingIndicator.isHidden = false
        self.loadingStatus.isHidden = false
        self.retryBtn.isHidden = true
        self.contentView.isHidden = true
        Alamofire.request(UsedURL.URL.domainUrl + UsedURL.Endpoints.apiCurrencyUrl , method : .get).responseObject{(response : DataResponse<CurrencyList>) in
            
            guard response.result.error == nil else
            {
                print(response.result.error ?? "Something Went Wrong")
                self.loadingStatus.text = "Something Went Wrong"
                self.retryBtn.isHidden = false
                return
            }
            
            guard let value = response.result.value else
            {
                self.loadingStatus.text = "Network Error"
                self.retryBtn.isHidden = false
                return
                
            }
            
            if value.info == "Central Bank of Myanmar"
            {
                self.currencyList = value
                self.tableView.reloadData()
                self.loadingView.isHidden = true
                self.loadingIndicator.isHidden = true
                self.loadingStatus.isHidden = true
                self.contentView.isHidden = false
            }
        }
    }
}
