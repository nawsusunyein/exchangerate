//
//  SettingViewController.swift
//  CBM Exchange
//
//  Created by techfun on 2018/01/23.
//  Copyright © 2018 techfun. All rights reserved.
//

import UIKit
import os.log

let APPLE_LANGUAGE_KEY = "AppleLanguages"
class SettingViewController: UIViewController
{
    
    @IBOutlet weak var menuBtn: UIBarButtonItem!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    // Language object creation to insert user chosen language.
    var language = Language()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // For Menu Appearance.
        if self.revealViewController() != nil
        {
            menuBtn.target = self.revealViewController()
            menuBtn.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        //To put segmented control with user selected language.
        let checkLanguage = LanguageHelper.loadLanguage(isRequire: true)!
        
        if checkLanguage.language == LanguageType.MM_Lang.rawValue
        {
            segmentedControl.selectedSegmentIndex = 1
        }
        else
        {
            segmentedControl.selectedSegmentIndex = 0
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func languageChange(_ sender: AnyObject) {
        let lang1 = Language(language : LanguageType.MM_Lang.rawValue)
        let lang2 = Language(language : LanguageType.EN_Lang.rawValue)
        
        if sender.selectedSegmentIndex == 1
        {
            language = lang1
        }
        else
        {
            language = lang2
        }
        
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(language, toFile: Language.ArchiveURL.path)
        if isSuccessfulSave
        {
            os_log("Language set successfully saved.", log: OSLog.default, type: .debug)
            LanguageHelper.language = language
            SettingViewController.setAppleLAnguageTo(lang: language.language!)
        }
        else
        {
            os_log("Failed to save language set...", log: OSLog.default, type: .error)
        }
        
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        
        //To indicate home view when user have chosen language.
        rootviewcontroller.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "rootnav")
    }
    
    // MARK: Get current apple language.
    class func currentAppleLanguage() -> String
    {
        let userdef = UserDefaults.standard
        let langArray = userdef.object(forKey: APPLE_LANGUAGE_KEY) as! NSArray
        let current = langArray.firstObject as! String
        return current
        
    }
    class func setAppleLAnguageTo(lang: String)
    {
        let userdef = UserDefaults.standard
        userdef.set([lang,currentAppleLanguage()], forKey: APPLE_LANGUAGE_KEY)
        userdef.synchronize()
    }
}
