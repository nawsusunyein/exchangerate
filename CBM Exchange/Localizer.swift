//
//  Localizer.swift
//  CBM Exchange
//
//  Created by techfun on 2018/01/24.
//  Copyright © 2018 techfun. All rights reserved.
//

import Foundation

class Localizer
{
    class func DoTheSwizzling()
    {
        
        MethodSwizzleGivenClassName(cls: Bundle.self, originalSelector: #selector(Bundle.localizedString(forKey:value:table:)), overrideSelector: #selector(Bundle.specialLocalizedStringForKey(key:value:table:)))
    }
}

extension Bundle
{
    func specialLocalizedStringForKey(key: String, value: String?, table tableName: String?) -> String
    {
        let currentLanguage = LanguageHelper.loadLanguage(isRequire: true)?.language!
        //let currentLanguage = SettingViewController.currentAppleLanguage()
        var bundle = Bundle();
        if let _path = Bundle.main.path(forResource: currentLanguage, ofType: "lproj")
        {
            bundle = Bundle(path: _path)!
        }
        else
        {
            let _path = Bundle.main.path(forResource: "Base", ofType: "lproj")!
            bundle = Bundle(path: _path)!
        }
        return (bundle.specialLocalizedStringForKey(key : key, value: value, table: tableName))
    }
}

/// Exchange the implementation of two methods for the same Class
func MethodSwizzleGivenClassName(cls: AnyClass, originalSelector: Selector, overrideSelector: Selector)
{
    let origMethod: Method = class_getInstanceMethod(cls, originalSelector);
    let overrideMethod: Method = class_getInstanceMethod(cls, overrideSelector);
    if (class_addMethod(cls, originalSelector, method_getImplementation(overrideMethod), method_getTypeEncoding(overrideMethod)))
    {
        class_replaceMethod(cls, overrideSelector, method_getImplementation(origMethod), method_getTypeEncoding(origMethod));
    }
    else
    {
        method_exchangeImplementations(origMethod, overrideMethod);
    }
}


