//
//  ExchangeRateDetailTableViewCell.swift
//  CBM Exchange
//
//  Created by techfun on 2018/01/23.
//  Copyright © 2018 techfun. All rights reserved.
//

import UIKit

class ExchangeRateDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var currencyTypeLbl: UILabel!
    
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var valueLbl: UILabel!
    
    @IBOutlet weak var exchangeRateLbl: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
}
