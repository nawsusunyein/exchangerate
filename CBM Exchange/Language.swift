//
//  Language.swift
//  CBM Exchange
//
//  Created by techfun on 2018/01/24.
//  Copyright © 2018 techfun. All rights reserved.
//

import Foundation

class Language : NSObject , NSCoding{
    
    struct LangKey
    {
        static let language = "language"
    }
    
    var language : String?
    
    init(language : String)
    {
        self.language = language
    }
    
    required override init()
    {
        
    }
    
    func encode(with aCoder: NSCoder)
    {
        aCoder.encode(language, forKey: LangKey.language)
        
    }
    
    required convenience init(coder aDecoder: NSCoder)
    {
        let language = aDecoder.decodeObject(forKey: LangKey.language) as? String
        self.init(language : language!)
    }
    
    // Create File Path
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("languages")
    
}
